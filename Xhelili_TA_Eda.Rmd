---
title: "Travel Audience Data Science Challenge"
output:
  html_document: default
  html_notebook: default
---
```{r, message=FALSE, warning=FALSE}
library(dplyr) 
library(ggplot2)
library(gridExtra)
library(lubridate)
```

# Read Data
There are 257K unique uuids, 17665 user agents and 270797 ips.
```{r}
df.logs <- read.csv('logs.csv', stringsAsFactors = F)
sapply(df.logs, function(x) length(unique(x)))
head(df.logs)
```

# Timestamp features
Compute features from timestamp:

  * Date
  * Hour
  * Day of the week
  * Impression occurs within workings hour (True if hour of time stamp is between 9:00 and 18:00)
 
The dataset contains one month of data from July 1 2017 to July 31 2017. The events are approximately uniformly distributed from 10:00 to 20:00. 

```{r}
df.logs <- df.logs%>%
  mutate(date = date(as.POSIXct(ts, origin="1970-01-01")),
         hour = hour(as.POSIXct(ts, origin="1970-01-01")),
         weekday = (weekdays(date)),
         is_weekend = weekday %in% c("Saturday", "Sunday"),
         working_hour = ifelse(hour <= 18 & hour >= 9 & is_weekend == F, T, F))
summary(df.logs)

```

```{r}
hist(df.logs$hour, main = "Impression Hour", xlab = "Hour")
```


Sunday and Monday are the days with the highest number of events. Most of the events happen outside of working hours (~58%).

```{r}
table(df.logs$weekday)/nrow(df.logs)
table(df.logs$working_hour)/nrow(df.logs)
```


Next, I group hours into different parts of the day. It would be interesting to compute the average Click Through Rate for different parts of the day.

```{r}
df.logs <- df.logs%>%
  mutate(time_of_day = case_when(.$hour %in% 5:8 ~ "EarlyMorning",
                                 .$hour %in% 9:10 ~ "Morning",
                                 .$hour %in% 11:20 ~ "Midday_Afternoon",
                                 .$hour %in% c(21:23, 0) ~ "Evening",
                                 .$hour %in% 1:4 ~"Night"))

table(df.logs$time_of_day)/nrow(df.logs)
```


Another way to group hours of the day based on "routine".

```{r}
df.logs <- df.logs%>%
  mutate(time_of_day_routine = case_when(.$hour %in% 5:10 ~ "WakingUp",
                                 .$hour %in% 11:18 ~ "Constant",
                                 .$hour %in% 19:20 ~ "AfterDinner",
                                 .$hour %in% c(21:23, 0:4) ~ "WindingDown"))
table(df.logs$time_of_day_routine)/nrow(df.logs)

```


Another feature that could be interesting to look at is the time between two consequitive impressions of the same user. This feature informs on how old a user interaction is compared to the user's last interaction. The missing values belong to the first event of a user. If the first event is ignored (NA values of time difference), the % of impressions that occur during the same minute is ~ 1.78%. The average time between two impressions is ~ 22.4 hours, while the median is only 1 minute. This indicates that the distribution is right skewed.

```{r}
df.logs <- df.logs%>%
  group_by(uuid)%>%
  arrange(ts)%>%
  mutate(time_diff_mins = as.integer(difftime(ts, lag(ts), units = "mins")))

df.logs <- df.logs%>%group_by(uuid)%>%mutate(n_events = n())

summary(df.logs$time_diff_mins)

# % of events occurring within the same minute
sum(df.logs$time_diff_mins == 0, na.rm = T)/nrow(df.logs[!is.na(df.logs$time_diff_mins),])

sum(df.logs$n_events == 1)/nrow(df.logs)
df.logs <- df.logs%>%
  mutate(time_diff_hours = time_diff_mins/60,
         time_diff_days  = time_diff_hours/24)

```


The histogram indeed shows the right skewed distribution of minutes between two impressions.

```{r}
hist(df.logs$time_diff_mins, main = "Time between Impressions (mins)", xlab = "Mins")
```


Taking the logarithm of the time difference doesn't help in removing the right skewness of the distribution.

```{r}
hist(log(1+df.logs$time_diff_mins), main = "Logarith of Time between Impressions (mins)", xlab = "Mins")
```

# Platform and OS Features

Given that the browsing experience differs between mobile, desktop and tablet platform I try to extract this information from the user agent predictor. A naive way of extracting this information leads to a coverage of only 28% of the records.

```{r}
mobile <- sum(grepl("mobile", tolower(df.logs$useragent)))/nrow(df.logs)
tablet <- sum(grepl("tablet", tolower(df.logs$useragent)))/nrow(df.logs)
desktop <- sum(grepl("desktop", tolower(df.logs$useragent)))/nrow(df.logs)

mobile + tablet + desktop
```

After exploring the data visually I noticed the occurrence of "iPad", "Windows", "Linux" in the user agent strings. 
It seems easier to extract the Operating System rather than the platform so I extract the feature OS with categories mac/windows/linux/other. I also add a binary feature: mobile (T/F). 28% of the impressions occur on mobile decices.

```{r}
ipad <- sum(grepl("ipad", tolower(df.logs$useragent)))/nrow(df.logs)
linux <- sum(grepl("linux", tolower(df.logs$useragent)))/nrow(df.logs)
mac <- sum(grepl("mac", tolower(df.logs$useragent)))/nrow(df.logs)
windows <- sum(grepl("windows", tolower(df.logs$useragent)))/nrow(df.logs)
linux + mac + windows

```

```{r}
df.logs$is_mobile <- as.logical(ifelse(grepl("mobile", tolower(df.logs$useragent)), T, F))
table(df.logs$is_mobile)/nrow(df.logs)
df.logs$os <- NA
df.logs$os <- sapply(df.logs$useragent, function(x){
  if(grepl("linux", tolower(x))){
    "Linux"
  }else if(grepl("mac", tolower(x))){
    "MacOs"
  }else if(grepl("windows", tolower(x))){
    "Windows"
  }else{
    "OTher"
  }
})
 
table(df.logs$os)/nrow(df.logs)
                                    
```


# UUID Features
Compute features per UUID

    *Number of events
    *Number of unique days
    *If a UUID traffic tends to be within weekday business hours
    *If traffic comes from a mobile device or not
    *Operating system of the device

A small percentage of UUIDs have more than 1 OS. I retreive the first OS for these instances.

```{r}
df.features <- df.logs%>%
  group_by(uuid)%>%
  summarise(events = n(),
            unique_days = n_distinct(date),
            weekday_events = sum(!is_weekend),
            weekend_events = sum(is_weekend),
            working_hours = sum(working_hour),
            nonworking_hours = sum(!working_hour),
            weekday_biz = ifelse(working_hours > nonworking_hours, T, F),
            os_count = n_distinct(os),
            os = os[1],
            is_mobile = is_mobile[1],
            avg_mins_to_impression = mean(time_diff_mins, na.rm = T))
summary(df.features)

options(scipen = 999)
sum(df.features$os_count > 1)/nrow(df.logs)


```

In order to decide on the threshold of number of events that determine if a user is highly active or not, I compute the cumulative percentage of UIIds with a certain number of events. The plot confirms that the number of events is very highly right skewed.


```{r}
df.events <- data.frame(no_events = unique(df.features$events))%>%arrange(no_events)
df.events$cum_perc <- sapply(df.events$no_events, function(x) sum(df.features$events <= x)/nrow(df.features))

max(df.events$cum_perc)

par(mfrow = c(2, 1))
plot(df.events$no_events, df.events$cum_perc*100, main = "Cumulative percentage of UUIDs with at most N events",
     xlab = "Number of Events", ylab = "% of UUIDs")
plot(df.events$no_events[1:10], df.events$cum_perc[1:10]*100, main = "Cumulative percentage of UUIDs with at most N events", xlab = "Number of Events", ylab = "% of UUIDs", type = 'b')

```


Percentiles give a more detailed view: 72% of UUIDs have 1 impression event, 90% up to 4 events. I will consider having 4 or more events as being a highly active user.


```{r}
head(df.events%>%arrange(no_events), n = 10)
```


```{r}
df.features <- df.features%>%
  mutate(highly_active = ifelse(events >= 4, T, F),
         multiple_days = ifelse(unique_days > 1, T, F))
summary(df.features)
```

# Visualise Extracted Features

Just 13% of the users are highly active, 14% of them had events ocurring on multiple days, 38% of their traffic tends to occur on weekday business hours, and 42% generate traffic from a mobile device. The most popular OS is MacOS followed by Windows. Most of the users are exposed to impressions within a short time span.


```{r}
table(df.features$highly_active)/nrow(df.features)
```

```{r}
table(df.features$multiple_days)/nrow(df.features)
```

```{r}
table(df.features$weekday_biz)/nrow(df.features)
```

```{r}
table(df.features$is_mobile)/nrow(df.features)
```

```{r}
p1 <- ggplot(df.features, aes(highly_active)) + geom_bar() +  ggtitle("Highly Active Users") +
  theme(plot.title = element_text(hjust = 0.5)) + xlab("Highly Active")

p2 <- ggplot(df.features, aes(multiple_days)) + geom_bar() +  ggtitle("Multiple Days Users") +
  theme(plot.title = element_text(hjust = 0.5)) + xlab("Mutliple Days")

p3 <- ggplot(df.features, aes(weekday_biz)) + geom_bar() +  ggtitle("Business Hours Users") +
  theme(plot.title = element_text(hjust = 0.5)) + xlab("Business Hours")

p4 <- ggplot(df.features, aes(is_mobile)) + geom_bar() +  ggtitle("Mobile Users") +
  theme(plot.title = element_text(hjust = 0.5)) + xlab("Mobile Users")

p5 <- ggplot(df.features, aes(os)) + geom_bar() +  ggtitle("OS") +
  theme(plot.title = element_text(hjust = 0.5)) + xlab("OS")

p6 <- ggplot(df.features, aes(avg_mins_to_impression)) + geom_histogram() +  ggtitle("Average Time to Impression (mins)") +
  theme(plot.title = element_text(hjust = 0.5)) + xlab("Time(mins)")
  
grid.arrange(p1, p2, p3, p4, p5, p6)

```

# Next Steps for Model Building
Click prediction is an unbalanced class problem because most users do not click on ads (low Click Through Rate). That being said, we could achieve a misclassification rate of 1-CTR% just by predicting 0. Both the True Positive Rate (recall, sensitivity) and the False Positive Rate (fall-out, specificity) need to be considered for evaluating the predictive performance.

Here are some features which I think would be useful for building a click prediction model:

  * Ad data: some ads are more popular than others. Extract features that describe this difference such as CTR statistics (mean, median, standard deviation/median absolute deviation/ kurtosis). I would also compute these statisics across different categories (e.g. per OS, platform etc.)
  * Temporal data: if the timeframe of the data set were longer, I would consider variables such as Date being the first Saturday of a month (close to pay day), closeness to major holidays (e.g. Xmas), recency of the ad (how recent the add is at the time of impression)
  * Geographical data: zip codes, zip groups, distance from relevant locations etc. 
  * Extract census data for geographical regions

Some of the methods that I could potentially use for feature engineering are:

  * Binning numerical variables
  * Transforming numerical features (log, scale, normalize, interactions)
  * Merging sparse categories for categorical data + one hot encoding
  * Instead of using categories, using e.g. count or average of the category
  * Extract trends: e.g. CTR last week/month/quarter/year

Some of the extracted features might simply add noise hence feature selection is important. I would combine a fast and crude feature selection method (e.g. correlation for continuous variables, t-scores for binary variables) with a regularized model. Feature importance provided by XGBoost could also be useful.

In terms of modeling: a good start is logistic regression (regularized version) and XGBoost. An ensemble of different models might further improve the prediction performance.